package com.GithubMeizhis;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 2017/9/7.
 */

public class MyMeizhiRetrofit {
    final GankApi gankService;

    public MyMeizhiRetrofit() {

        Retrofit retrofit = new Retrofit.Builder()
//                .client()
                .baseUrl("http://gank.io/api/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        gankService = retrofit.create(GankApi.class);
    }

    public GankApi getGankService() {
            return gankService;
        }
}
