package com.GithubMeizhis;

import com.GithubMeizhis.entity.GankData;
import com.GithubMeizhis.entity.MeizhiData;
import com.GithubMeizhis.entity.休息视频Data;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Administrator on 2017/9/7.
 */

public interface GankApi {
    @GET("data/福利/" + 10 + "/{page}")
    Observable<MeizhiData> getMeizhiData(@Path("page") int page);
//    Call<List<Meizhi>> getMeizhiData(@Path("page") int page);


    @GET("data/休息视频/" + 10 + "/{page}")
    Observable<休息视频Data> get休息视频Data(@Path("page") int page);

    @GET("day/{year}/{month}/{day}") Observable<GankData> getGankData(
            @Path("year") int year,
            @Path("month") int month,
            @Path("day") int day);


}
