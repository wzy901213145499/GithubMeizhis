package com.GithubMeizhis;

import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.GithubMeizhis.base.ToolbarActivity;
import com.GithubMeizhis.util.AXLog;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;

public class PicActivity extends ToolbarActivity {


    @Bind(R.id.picture)
    ImageView mImageView;
    public static final String EXTRA_IMAGE_URL = "image_url";
    public static final String EXTRA_IMAGE_TITLE = "image_title";
    public static final String TRANSIT_PIC = "picture";
    private String mImageUrl;
    private String mImageTitle;

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_picture;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AXLog.e("wzytest","run in  onCreate1");
        ButterKnife.bind(this);
        parseIntent();
        Glide.with(this)
                .load(mImageUrl)
                .into(mImageView);
        setAppBarAlpha(0.7f);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        AXLog.e("wzytest","run in  onSaveInstanceState");

    }


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        AXLog.e("wzytest","run in  onCreate2");
        ButterKnife.bind(this);
        parseIntent();
        Glide.with(this)
                .load(mImageUrl)
                .into(mImageView);
    }

    private void parseIntent() {
        mImageUrl = getIntent().getStringExtra(EXTRA_IMAGE_URL);
        mImageTitle =  getIntent().getStringExtra(EXTRA_IMAGE_TITLE);
        AXLog.e("wzytest","mImageUrl:"+mImageUrl);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pic, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save:
                RxMeizhi.saveImageAndGetPathObservable(this,mImageUrl,mImageTitle)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Uri>() {
                            @Override
                            public void onCompleted() {
                                AXLog.e("wzytest","run in onCompleted");
                            }

                            @Override
                            public void onError(Throwable e) {
                                AXLog.e("wzytest","run in onError");
                            }

                            @Override
                            public void onNext(Uri uri) {
                                AXLog.e("wzytest","run in onNext");
                            }
                        });
                break;
            case R.id.share:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
