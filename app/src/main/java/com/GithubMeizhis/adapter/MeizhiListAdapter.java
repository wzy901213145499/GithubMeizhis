package com.GithubMeizhis.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.GithubMeizhis.MeizhiViewHolderOnclick;
import com.GithubMeizhis.R;
import com.GithubMeizhis.entity.Meizhi;
import com.GithubMeizhis.widget.RatioImageView;

import java.util.List;

/**
 * Created by Administrator on 2017/9/11.
 */

public class MeizhiListAdapter extends RecyclerView.Adapter<MeizhiListAdapter.MyViewHolder> {
    private List<Meizhi> meizhis;
    private Context context;
    MeizhiViewHolderOnclick onClickListener;


    public MeizhiListAdapter(Context context,List<Meizhi> meizhis){
        this.context = context;
        this.meizhis = meizhis;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_meizhi, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Meizhi meizhi = meizhis.get(position);
        int limit = 48;
        String text = meizhi.desc.length() > limit ? meizhi.desc.substring(0, limit) +
                "..." : meizhi.desc;
        holder.meizhiDesc.setText(text);
        holder.meizhi = meizhi;
        holder.url = meizhi.url;
        Glide.with(context)
                .load(meizhi.url)
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_github)
                .into(holder.meizhiImg);
//                .getSize((w,h)->{
//                    if (!holder.card.isShown()) {
//                        holder.card.setVisibility(View.VISIBLE);
//                    }
//                });
    }


    @Override
    public int getItemCount() {
        return meizhis.size();
    }

    public void setOnItemclickListner(MeizhiViewHolderOnclick onclickListner){
        this.onClickListener = onclickListner;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        RatioImageView meizhiImg;
        TextView meizhiDesc;
        View card;
        String url;
        Meizhi meizhi;

        public MyViewHolder(View itemView) {
            super(itemView);
            card = itemView;
            meizhiImg = (RatioImageView) itemView.findViewById(R.id.iv_meizhi_item);
            meizhiDesc = (TextView) itemView.findViewById(R.id.tv_meizhi_item);
            //设置宽高相等
            meizhiImg.setOriginalSize(50,50);
            meizhiImg.setOnClickListener(this);
            meizhiDesc.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            onClickListener.onMeiZhiclik(view,url,meizhi);
        }
    }
}
