package com.GithubMeizhis.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.GithubMeizhis.R;
import com.GithubMeizhis.WebActivity;
import com.GithubMeizhis.entity.Gank;
import com.GithubMeizhis.util.AXLog;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/9/20.
 */

public class GankListAdapter extends Adapter<GankListAdapter.ViewHolder> {


    private List<Gank> mGankList;

    public GankListAdapter(List<Gank> ganks) {
        mGankList = ganks;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_gank, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Gank gank = mGankList.get(position);
        if (position == 0) {
            holder.category.setVisibility(View.VISIBLE);
        } else {
            if (gank.type.equals(mGankList.get(position - 1).type)) {
                holder.category.setVisibility(View.GONE);
            } else {
                holder.category.setVisibility(View.VISIBLE);
            }
        }
        holder.category.setText(gank.type);
        holder.gank.setText(gank.desc);

    }


    private void showCategory(ViewHolder holder) {
        if (!isVisibleOf(holder.category)) holder.category.setVisibility(View.VISIBLE);
    }

    /**
     * view.isShown() is a kidding...
     */
    private boolean isVisibleOf(View view) {
        return view.getVisibility() == View.VISIBLE;
    }


    @Override
    public int getItemCount() {
        return mGankList.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.category)
        TextView category;
        @Bind(R.id.title)
        TextView gank;
//        @Bind(R.id.gank_layout)
//        LinearLayout gankLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            AXLog.e("wzytest", "run in ViewHolder:" + category + ":" + gank);
        }

        @OnClick(R.id.gank_layout)
        public void onViewClicked(View v) {
            Gank gank = mGankList.get(getLayoutPosition());
            Intent intent = WebActivity.newIntent(v.getContext(), gank.url, gank.desc);
            v.getContext().startActivity(intent);
        }
    }
}
