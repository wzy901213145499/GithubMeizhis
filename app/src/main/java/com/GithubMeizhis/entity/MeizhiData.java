/*
 * Copyright (C) 2015 Drakeet <drakeet.me@gmail.com>
 *
 * This file is part of Meizhi
 *
 * Meizhi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Meizhi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Meizhi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.GithubMeizhis.entity;

import com.litesuits.orm.db.annotation.Table;

import java.util.List;

/**
 * Created by drakeet on 6/20/15.
 */
@Table("meizhis") public class MeizhiData extends Soul {

    public List<Meizhi> results;


//            "_id": "599386fe421aa9672cdf0812",
//            "createdAt": "2017-08-16T07:42:54.135Z",
//            "desc": "8-16",
//            "publishedAt": "2017-08-17T11:36:42.967Z",
//            "source": "chrome",
//            "type": "\u798f\u5229",
//            "url": "https://ws1.sinaimg.cn/large/610dc034ly1fil82i7zsmj20u011hwja.jpg",
//            "used": true,
//            "who": "daimajia"



}
