package com.GithubMeizhis.widget;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.GithubMeizhis.util.AXLog;

/**
 * Created by Administrator on 2017/9/12.
 */

public class FABbehavior extends FloatingActionButton.Behavior {
    boolean outAnimend = true;

    public FABbehavior(Context context, AttributeSet attrs) {
        super();
        AXLog.e("wzytest","run in fab 构造方法");
    }

//    @Override
//    public boolean layoutDependsOn(CoordinatorLayout parent, FloatingActionButton child, View dependency) {
//        return super.layoutDependsOn(parent, child, dependency);
//    }
//
//    @Override
//    public boolean onDependentViewChanged(CoordinatorLayout parent, FloatingActionButton child, View dependency) {
//        return super.onDependentViewChanged(parent, child, dependency);
//    }


    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionButton child, View directTargetChild, View target, int nestedScrollAxes) {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL ||
                super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target,
                        nestedScrollAxes);
    }

//    @Override
//    public void onNestedScroll(CoordinatorLayout coordinatorLayout, FloatingActionButton child, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
//        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
//        AXLog.e("wzytest","run in onNestedScroll :"+dyConsumed+":"+dxUnconsumed);
//    }

    @Override
    public void onNestedScroll(final CoordinatorLayout coordinatorLayout, final FloatingActionButton child, final View target, final int dxConsumed, final int dyConsumed, final int dxUnconsumed, final int dyUnconsumed) {
        AXLog.e("wzytest","run in onNestedScroll :"+dyConsumed+":"+dxUnconsumed);

        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed,
                dyUnconsumed);
        if (dyConsumed > 0 &&
                child.getVisibility() == View.VISIBLE && outAnimend) {
            AXLog.e("wzytest","动画浅出111");
            // User scrolled down and the FAB is currently visible -> hide the FAB
            Animation anim = AnimationUtils.loadAnimation(child.getContext(),
                    android.support.design.R.anim.design_fab_out);
            anim.setDuration(200L);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                     outAnimend = false;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    outAnimend = true;
                    child.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            child.setAnimation(anim);
        } else if (dyConsumed < 0 && child.getVisibility() != View.VISIBLE) {
            // User scrolled up and the FAB is currently not visible -> show the FAB
            AXLog.e("wzytest","动画浅入");
            child.setVisibility(View.VISIBLE);
            Animation anim = AnimationUtils.loadAnimation(child.getContext(),
                    android.support.design.R.anim.design_fab_in);
            child.setAnimation(anim);
        }
    }
}
