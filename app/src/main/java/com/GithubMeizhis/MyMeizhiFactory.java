package com.GithubMeizhis;

/**
 * Created by Administrator on 2017/9/7.
 */

public class MyMeizhiFactory {
    private static GankApi sGankIOSingleton = null;

    public static GankApi getsGankIOSingleton() {
        if(sGankIOSingleton==null){
            sGankIOSingleton = new MyMeizhiRetrofit().getGankService();
        }
        return sGankIOSingleton;
    }
}
