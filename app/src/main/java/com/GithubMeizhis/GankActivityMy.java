package com.GithubMeizhis;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.GithubMeizhis.adapter.GankPagerAdapter;
import com.GithubMeizhis.base.ToolbarActivity;
import com.GithubMeizhis.entity.Gank;
import com.GithubMeizhis.util.AXLog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GankActivityMy extends ToolbarActivity {

    public static final String EXTRA_GANK_DATE = "gank_date";
    public static final String EXTRA_IMG_DATE = "img_date";
    GankPagerAdapter mPagerAdapter;
    List<Gank> mGankList = new ArrayList<Gank>();
    Date mGankDate;

    @Bind(R.id.pager)
    ViewPager viewPager;

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_gank;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mGankDate = (Date) getIntent().getSerializableExtra(EXTRA_GANK_DATE);
        mPagerAdapter =  new GankPagerAdapter(getSupportFragmentManager(),mGankDate);
        AXLog.e("wzytest","mPagerAdapter.size:"+mPagerAdapter.getCount());
        viewPager.setAdapter(mPagerAdapter);
        mPagerAdapter.notifyDataSetChanged();
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
//
//    private List<Gank> addAllResults(GankData.Result results) {
//        if (results.androidList != null) mGankList.addAll(results.androidList);
//        if (results.iOSList != null) mGankList.addAll(results.iOSList);
//        if (results.appList != null) mGankList.addAll(results.appList);
//        if (results.拓展资源List != null) mGankList.addAll(results.拓展资源List);
//        if (results.瞎推荐List != null) mGankList.addAll(results.瞎推荐List);
//        if (results.休息视频List != null) mGankList.addAll(0, results.休息视频List);
//        AXLog.e("wzytest","results:"+mGankList.size() );
//        return mGankList;
//    }
//
//    public void loadGankDta(){
//       MyMeizhiFactory.getsGankIOSingleton().getGankData(2017,9,1)
//                .map(gankData -> gankData.results)
//                .map( result -> addAllResults(result))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(
//
//                        new Observer<List<Gank>>() {
//                            @Override
//                            public void onCompleted() {
//                                AXLog.e("wzytest","run in onCompleted");
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//                                AXLog.e("wzytest","run in onError");
//                            }
//
//                            @Override
//                            public void onNext(List<Gank> ganks) {
//                                AXLog.e("wzytest","run in onNext" +
//                                        "");
//                            }
//                        }
//                );
//    }

}
