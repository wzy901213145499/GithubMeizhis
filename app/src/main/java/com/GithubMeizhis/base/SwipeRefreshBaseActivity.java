/*
 * Copyright (C) 2015 Drakeet <drakeet.me@gmail.com>
 *
 * This file is part of Meizhi
 *
 * Meizhi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Meizhi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Meizhi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.GithubMeizhis.base;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;

import com.GithubMeizhis.R;
import com.GithubMeizhis.util.AXLog;
import com.GithubMeizhis.widget.MultiSwipeRefreshLayout;


/**
 * Created by drakeet on 1/3/15.
 */
public abstract class SwipeRefreshBaseActivity extends BaseActivity
        implements SwipeRefreshLayer {

//    @Bind(R.id.swipe_refresh_layout)
    public MultiSwipeRefreshLayout mSwipeRefreshLayout;

    private boolean mIsRequestDataRefresh = false;


//    @Override
//    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
//        super.onCreate(savedInstanceState, persistentState);
//        ButterKnife.bind(this);
//
//        if(mSwipeRefreshLayout==null)
//        AXLog.e("wzytest","mSwipeRefreshLayout is null");
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //ButterKnife.bind(this);
        mSwipeRefreshLayout = (MultiSwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        if(mSwipeRefreshLayout==null)
            AXLog.e("wzytest","mSwipeRefreshLayout is null");
        else
            AXLog.e("wzytest","mSwipeRefreshLayout is not null");
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        trySetupSwipeRefresh();
    }


    void trySetupSwipeRefresh() {
        AXLog.e("wzytest","run in trySetupSwipeRefresh");

        if (mSwipeRefreshLayout != null) {
            AXLog.e("wzytest","mSwipeRefreshLayout != null");
            mSwipeRefreshLayout.setColorSchemeResources(R.color.refresh_progress_3,
                    R.color.refresh_progress_2, R.color.refresh_progress_1);
            // Do not use lambda here!
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    requestDataRefresh();
                }
            });

        }
    }


    @Override
    public void requestDataRefresh() {
        AXLog.e("wzytest","run in requestDataRefresh");
        mIsRequestDataRefresh = true;
    }


    public void setRefresh(boolean requestDataRefresh) {

        if (mSwipeRefreshLayout == null) {
            AXLog.e("wzytest","mSwipeRefreshLayout == null");
            return;
        }
        if (!requestDataRefresh) {
            mIsRequestDataRefresh = false;
            // 防止刷新消失太快，让子弹飞一会儿.
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mSwipeRefreshLayout != null) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
            }, 500);
        } else {
            AXLog.e("wzytest","requestDataRefresh");
            mSwipeRefreshLayout.setRefreshing(true);
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mSwipeRefreshLayout != null) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
            }, 2000);
        }
    }


    @Override
    public void setProgressViewOffset(boolean scale, int start, int end) {
        mSwipeRefreshLayout.setProgressViewOffset(scale, start, end);
    }



    public boolean isRequestDataRefresh() {
        return mIsRequestDataRefresh;
    }
}
