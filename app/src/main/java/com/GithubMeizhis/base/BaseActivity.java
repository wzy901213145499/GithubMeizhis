package com.GithubMeizhis.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import butterknife.ButterKnife;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BaseActivity extends AppCompatActivity {
//    public abstract int getContentViewId();

    private CompositeSubscription mCompositeSubscription;
    private Dialog mDialog;

    //rxjava  获取所有的订阅
    public CompositeSubscription getCompositeSubscription() {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }

        return this.mCompositeSubscription;
    }

    //rxjava  增加订阅
    public void addSubscription(Subscription s) {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }

        this.mCompositeSubscription.add(s);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(provideContentViewId());
        ButterKnife.bind(this);
//        initAllMembersView(savedInstanceState);
//        EventBus.getDefault().register(this);
//        ActivityCollector.addActivity(this);
//        ChooseLanguageApplication.DefaulteChooselanguage(this);
//        StatusBarUtil.setTransparent(this);

    }

    protected abstract int provideContentViewId();

//    protected abstract void initAllMembersView(Bundle savedInstanceState);

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        EventBus.getDefault().unregister(this);
//        ActivityCollector.removeActivity(this);
        // rxjava 解除订阅
        if (this.mCompositeSubscription != null) {
            this.mCompositeSubscription.unsubscribe();
        }

    }

//    @Subscribe(threadMode = ThreadMode.POSTING)
//    public void onEvent(String str) {
//        switch (str) {
//            case ChooseLanguageActivity.EVENT_REFRESH_LANGUAGE:
//                ChooseLanguageApplication.DefaulteChooselanguage(this);
//                recreate();//刷新界面
//                break;
//        }
//    }

    public  void showRoundProcessDialog(Context mContext, int layout)
    {

        DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener()
        {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
            {
                if (keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_SEARCH)
                {
                    return true;
                }
                return false;
            }
        };
        mDialog = new AlertDialog.Builder(mContext).create();
        mDialog.setOnKeyListener(keyListener);

        mDialog.show();
        // 注意此处要放在show之后 否则会报异常
        mDialog.setContentView(layout);
    }

    public void hideRoundProcessDialog(){
        if(mDialog!=null&&mDialog.isShowing()){
            mDialog.dismiss();
        }
    }
}